///\file deviceprop.hh
///\brief contient la définition de la classe Info::DeviceProp
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#ifndef _DEVICEPROP
#define _DEVICEPROP

#include <cuda.h>

///\namespace Info
///\brief namespace pour les informations liées au matériel
namespace Info{

  ///\class DeviceProp
  ///\brief extrait les informations liées au GPU
  class DeviceProp{
  private :
    cudaDeviceProp devProp; ///< Structure de propriété GPU de CUDA
    bool human; ///< affichage ou ecriture dans un fichier
    int devCount; ///< Nombre de cartes graphiques
  
  public:
    ///\brief Constructeur
    ///\param h définir affichage ou écriture dans un fichier
    DeviceProp(bool h=false);
    
    ///\brief permet de recuperer les infos 
    void get_info();

    ///\brief permet de trouver le nombre de MPS sur le GPU
    unsigned int get_nb_mps();

  };
}

#endif
