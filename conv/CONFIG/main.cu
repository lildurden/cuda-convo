///\file main.cu
///\brief programme principal pour la configuration
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#include "deviceprop.hh"

///\brief programme principal
int main(){
  Info::DeviceProp dp(false);
  dp.get_info();
}
