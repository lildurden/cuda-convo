///\file deviceprop.cu
///\brief contient les implémentations des méthodes de Info::DeviceProp
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#include "deviceprop.hh"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

namespace Info{

  DeviceProp::DeviceProp(bool h):human(h){
    cudaGetDeviceCount(&devCount);
  }

  void DeviceProp::get_info(){

    ostringstream oss;

    //On ne gère pas les 2 CG
    //for(unsigned int i = 0;i<devCount;++i){
    
    //Retirer si boucle for...
    int i=0;

    cudaGetDeviceProperties(&devProp, i);

    if(!human) oss<<"//";
    oss<<"Card "<<i<<" : "<<devProp.name<<endl;

    if(human) oss<<"Memoire GPU totale : ";
    else oss<<"#define GPU_TOTAL_MEM "<<devProp.totalGlobalMem<<endl; 

    if(human) oss<<"Memoire shared par block : ";
    else oss<<"#define BLOCK_SHARED_MEM "<<devProp.sharedMemPerBlock<<endl;

    if(human) oss<<"Memoire registre par block : ";
    else oss<<"#define BLOCK_REG_MEM "<<devProp.regsPerBlock<<endl;

    if(human) oss<<"Nombre de threads par block : ";
    else oss<<"#define PHYSICAL_NB_THREADS "<<devProp.warpSize<<endl;

    if(human) oss<<"Nombre de Multi Processeurs : ";
    else oss<<"#define PHYSICAL_MPS "<<devProp.multiProcessorCount<<endl;
    //  }


    if(human) cout<<oss.str();
    else {
      std::ofstream file("gpu.hh", ios::out);
      if(file){
	file<<oss.str();
	file.close();
      }
      else cerr<<"Cannot get GPU info !"<<endl;
    }

  }

  unsigned int DeviceProp::get_nb_mps(){
    return devProp.multiProcessorCount;
  }

}
