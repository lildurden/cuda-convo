///\file s_convolution.hh
///\brief contient les spécifications de la classe Simple::Convolution
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#ifndef _SIMPLE_CONVOLUTION
#define _SIMPLE_CONVOLUTION

#include "convolution.hh"

///\namespace Simple
///\brief contient les classes de traitement d'image en mode séquentiel
namespace Simple{
  ///\class Convolution
  ///\brief moteur pour effectuer des convolutions séquentielles sur des Images
  class Convolution: public Generic::Convolution{
  private :
   
    void calc(pixel &p,const Image &image,unsigned int y,unsigned int x) const;

  public :
    ///\brief Constructeur
    ///construit un moteur de convolution séquentiel
    ///\param m matrice de convolution
    ///\param d diviseur à appliquer
    Convolution(const Matrix<int> &m, unsigned int d);

    ///\brief effectue une convolution séquentielle sur une image
    ///\param image image en entrée
    ///\param result image en sortie
    void convol(const Image& image, Image& result) const;
  };
}

#endif
