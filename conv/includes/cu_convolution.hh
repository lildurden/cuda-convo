///\file cu_convolution.hh
///\brief contient les spécifications de la classe CUDA::Convolution
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#ifndef _CUDA_CONVOLUTION
#define _CUDA_CONVOLUTION

#include "convolution.hh"

///\namespace CUDA
///\brief contient les classes de traitement d'image en mode CUDA
namespace CUDA{
  ///\class Convolution
  ///\brief moteur pour effectuer des convolutions CUDA sur des Images
  class Convolution: public Generic::Convolution{
  private :
    
  public :
    ///\brief Constructeur
    ///construit un moteur de convolution CUDA
    ///\param m matrice de convolution
    ///\param d diviseur à appliquer
    Convolution(const Matrix<int> &m, unsigned int d);

    ///\brief effectue une convolution CUDA sur une image
    ///\param image image en entrée
    ///\param result image en sortie
    void convol(const Image& image, Image& result) const;
  };
}

#endif
