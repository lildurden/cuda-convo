///\file convolution.hh
///\brief contient les spécifications de la classe Convolution
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#ifndef _CONVOLUTION
#define _CONVOLUTION

#include "matrix.hh"
#include "image.hh"

///\namespace Generic
///\brief contient les définitions abstraites des classes de traitement d'image
namespace Generic{
  ///\class Convolution
  ///\brief moteur pour effectuer des convolutions sur des Images
  class Convolution{
  private:
    protected :
    const Matrix<int> &m; ///< Matrice de convolution
    const unsigned int divider; ///< Diviseur à appliquer après sommage
  
  public :
    ///\brief Constructeur
    ///construit un moteur de convolution CUDA
    ///\param m matrice de convolution
    ///\param d diviseur à appliquer
    Convolution(const Matrix<int> &m, unsigned int d);

    ///\brief effectue une convolution sur une image
    ///\param image image en entrée
    ///\param result image en sortie
    virtual void convol(const Image& image, Image& result) const=0;
  };
}

#endif

#include "hardware.hh"
