///\file mt_convolution.hh
///\brief contient les spécifications de la classe MThread::Convolution
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier }
#ifndef _MTHREAD_CONVOLUTION
#define _MTHREAD_CONVOLUTION

#include "convolution.hh"

///\namespace MThread
///\brief contient les classes de traitement d'image en mode multi-thread
namespace MThread{
  ///\class Convolution
  ///\brief moteur pour effectuer des convolutions parallèles sur des Images
  class Convolution: public Generic::Convolution{
  private :
    ///\struct convargs
    ///\brief Arguments nécessaires à un thread de convolution
    struct convargs{
      const MThread::Convolution *c; ///< Convolution appelante
      const Image* image;   ///< Image d'entrée
      Image *result;        ///< Image de sortie
      unsigned int start;   ///< début de la zone de traitement sur y
      unsigned int end;     ///< fin de la zone de traitement sur y
    };
    
    ///\brief effectue le calcul d'un pixel à partir de ses coordonnées
    ///\param p pixel à écrire
    ///\param image image d'origine
    ///\param y position en y dans l'image
    ///\param x position en x dans l'image
    void calc(pixel& p,const Image &image,unsigned int y, unsigned int x) const;

    ///\brief appel séquentiel d'une convolution
    ///cette fonction est transmise aux threads pour le parallèlisme
    ///param v cast en void* d'un pointeur sur convargs
    static void* sconvol(void *v);

  public :
    ///\brief Constructeur
    ///construit un moteur de convolution multithreadé
    ///\param m matrice de convolution
    ///\param d diviseur à appliquer
    Convolution(const Matrix<int> &m, unsigned int d);
    
    ///\brief effectue une convolution parallèle sur une image
    ///\param image image en entrée
    ///\param result image en sortie
    void convol(const Image& image, Image& result) const;
  };
}

#endif
