///\file s_convolution.cpp
///\brief contient les implémentations de la classe Simple::Convolution
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier } 
#include "s_convolution.hh"

namespace Simple{
  Convolution::Convolution(const Matrix<int> &m, unsigned int d): Generic::Convolution(m,d) {}

  void Convolution::convol(const Image& im, Image& re) const{
    for(unsigned int i = m.height()/2; i < im.height()-m.height()/2; i++)
      for(unsigned int j = m.width()/2; j < im.width()-m.width()/2; j++)
	calc(re[i][j], im, i, j);
  }

  void Convolution::calc(pixel &p, const Image &image, 
			 unsigned int y, unsigned int x) const
  {
    float r = 0;
    float g = 0;
    float b = 0;
    float a = 0;
    x -= m.width()/2;
    y -= m.height()/2;
    for(unsigned int i = 0; i < m.height(); i++)
      for(unsigned int k = 0; k < m.width(); k++){
	r += image[y+i][x+k].r*m[i][k];
	g += image[y+i][x+k].g*m[i][k];
	b += image[y+i][x+k].b*m[i][k];
	a += image[y+i][x+k].a*m[i][k];
      }

    p.r = (r > 0)*(r/divider);
    p.g = (g > 0)*(g/divider);
    p.b = (b > 0)*(b/divider);
    p.a = (a > 0)*(a/divider);
  }
}
