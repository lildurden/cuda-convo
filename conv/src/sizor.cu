///\file sizor.cu
///\brief contient les implémentations de la classe CUDA::Sizor
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier } 
#include "sizor.hh"

namespace CUDA{

  Sizor::Sizor(const Image &data, const Matrix<int> &m_conv){
    unsigned int start=m_conv.width()/2;

    //Calcul de la taille des blocks in/out
    wblockmax_out = NB_THREADS*MPS;
    wblock_in = wblockmax_out+(2*start); 

    hblock_in = MAXMEM/(wblock_in*sizeof(pixel));
    hblockmax_out = hblock_in-(2*start);

    block_size = wblock_in*hblock_in;

    //Calcul du nombre de  zones de traitement (h/w)
    unsigned int x = (data.width()-2*start)/wblockmax_out + (((data.width()-2*start)%wblockmax_out) > 0);
    unsigned int y = (data.height()-2*start)/hblockmax_out + (((data.height()-2*start)%hblockmax_out) > 0);
    
    //Pour toutes les zones
    for(unsigned int i = 0; i < y; i++)
      for(unsigned int j = 0; j < x; j++){
	//Calculer les coordonnées début/fin ...
	coord_data c;
	
	c.bxo = start+j*wblockmax_out;
	c.bxi = c.bxo-start;

	c.byo = start+i*hblockmax_out;
	c.byi = c.byo-start;
	
	if(j == x-1)
	  c.wblock_out = data.width()-start-c.bxo;
	else
	  c.wblock_out = wblockmax_out;

	if(i == y-1)
	  c.hblock_out = data.height()-start-c.byo;
	else
	  c.hblock_out = hblockmax_out;

	c.hblock_in = c.hblock_out+2*start;
	c.wblock_in = c.wblock_out+2*start;
 
	block_lst.push_back(c);

      }

    it=block_lst.begin();
  }


}
