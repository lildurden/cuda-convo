///\file main.cpp
///\brief contient le programme principal
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier } 
#include "matrix.hh"
#include "image.hh"
#include "convolution.hh"

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>

int main(int argc, char* argv[]){
  if(!argv[1]){
    std::cerr<<"usage : "<<argv[0]<<" image.format"<<std::endl;
    exit(1);
  }

  clock_t start = clock();
  Image image(argv[1]);
  
  Matrix<int> matrix(3, 3);
  /*for(unsigned int i = 0; i < matrix.height(); i++)
    for(unsigned int j = 0; j < matrix.width(); j++)
      if     (i == 0 ) matrix[i][j] = -1;
      else if(i == 2 ) matrix[i][j] =  1;
      else            matrix[i][j] =  0;*/

  matrix[0][0] = 2;
  matrix[0][1] = 1;
  matrix[0][2] = 0;
  matrix[1][0] = 1;
  matrix[1][1] = 0;
  matrix[1][2] = -1;
  matrix[2][0] = 0;
  matrix[2][1] = -1;
  matrix[2][2] = -2;

  Convolution engine(matrix, 1);

  Image result(image.width(), image.height());
  clock_t load = clock();
  std::cout<<(double)(load-start)/CLOCKS_PER_SEC<<std::endl;
  

  std::cout<<"START"<<std::endl;
  engine.convol(image, result);
  std::cout<<"END"<<std::endl;
  
  clock_t save = clock();
  std::string name = argv[1];
  name = "conv-"+name;
  result.save(name.c_str());
  std::cout<<(double)(clock()-save)/CLOCKS_PER_SEC<<std::endl;
}
