\chapter{Introduction au domaine}

Ces travaux sont à l'intersection de deux domaines:
\begin{itemize}
 \item La programmation massivement multiprocesseur avec la technologie CUDA
\item Le traitement par matrice de convolution et assimilé.
\end{itemize}

\section{CUDA}

CUDA pour Compute Unified Device Architecture, est une technologie développée par NVidia. Elle est basée sur une architecture de type GPU \footnote{GPU : Graphics Processing Unit} massivement multiprocesseur ainsi que sur une bibliothèque permettant son utilisation. Cette technologie est supportée par les derniers modèles de carte graphique NVidia.

\subsection{Les origines de CUDA}

Dans un premier temps, l'accroissement de la puissance de calcul des processeurs consistait à augmenter le nombre de transistors, réduisant régulièrement la taille de gravure des processeurs. À partir de 2004, cette solution posa des problèmes de dissipation thermique. Les constructeurs de processeurs s'orientèrent donc vers l'augmentation du nombre de processeurs par puce. En 2006, avec la GeForce 8800 GTX, NVidia sort la première carte graphique possédant un GPU avec une architecture CUDA.

\subsection{Architecture}

\subsubsection{Hiérarchisation des threads}

\paragraph{}Pour effectuer des calculs sur les GPU CUDA nous emploierons des kernels. Un kernel est une portion de code à exécuter en parallèle sur la carte graphique. Chaque instance du kernel est donc un thread. Les threads sont regroupés par blocs. Chaque bloc est indépendant ce qui implique que les threads ne peuvent pas communiquer directement avec les threads des autres blocs. Les GPU CUDA sont composés de multiprocesseurs regroupés en blocs, cet ensemble forme une grille. Chaque bloc est composé de processeurs (voir figure \ref{fig:archi})

\newpage
\begin{figure}[h!]
  \centering
  \includegraphics[height=9cm,width=14cm]{images/machinkicontientdestrucsenglobedebidul.png}
  \caption{Schéma de l'architecture CUDA.}
  \label{fig:archi}
\end{figure}

\paragraph{}Un warp est un ensemble de $N$ thread, avec $N$ correspondant au nombre de processeurs présent dans un multiprocesseur. Chaque thread d'un warp est exécuté simultanément et sur 2 cycles. Le warp permet d'établir un parallèle avec le matériel. Chaque thread correspond à un processeur : SP \footnote{SP : Streaming Processor}, un bloc à un multiprocesseur : SM \footnote{SM : Streaming Multiprocessor} et la grille à l'ensemble du GPU.\\

\begin{figure}[h!]
  \centering
  \includegraphics[height=7cm,width=12cm]{images/grille-bloc.png}
  \caption{Hiérarchisation des threads avec une grille en 2 dimensions et des blocs en 3 dimensions.}
\end{figure}

\subsubsection{Répartition des mémoires}

Une carte graphique basée sur l'architecture CUDA possède plusieurs niveaux de mémoire.

\paragraph{}Tout d'abord la mémoire globale qui est accessible en lecture et en écriture par la grille, ainsi que par le CPU. Elle est de grande taille mais plutôt lente.

\paragraph{}Ensuite la mémoire constante qui est accessible en lecture et en écriture par le CPU et en lecture uniquement par la grille. Elle offre des temps d'accès plus rapides pour les threads que la mémoire globale et ses accès peuvent être effectués en parallèle.

\paragraph{}La mémoire shared\footnote{Mémoire partagée qui est accessible par plusieurs unités de calculs.} est accessible en écriture et lecture depuis l'ensemble des threads d'un bloc. Chaque thread d'un bloc ne peut accéder qu'à la mémoire shared de son bloc. Elle est divisée en banques. Chaque thread peut accéder simultanément à des banques différentes. Si deux threads tentent d'accéder simultanément à une même banque, il y a un conflit de banque et les accès sont sérialisés. Cette mémoire offre des temps d'accès proche de ceux des registres, donc très rapides.

\paragraph{}Enfin, chaque thread possède des registres. Chaque thread ne peut accéder qu'à ses propres registres. Dans la majorité des cas, l'accès à un registre ne coûte pas de cycle supplémentaire. Ils sont, de par leur nature, de taille assez faible.\\

\begin{figure}[h!]
  \centering
  \includegraphics[height=8cm,width=8cm]{images/memoire.png}
\caption{Répartition de la mémoire sur une carte graphique reposant sur une architecture CUDA}
\end{figure}

\subsection{Les outils fournis pour utiliser CUDA}

CUDA fournit des outils pour pouvoir développer des applications qui tirent partie de l'architecture CUDA. Ces outils sont :
\begin{itemize}
\item un pilote
\item un SDK\footnote{Software Development Kit}
\item des bibliothèques
\end{itemize}

Il est possible de développer des applications qui reposent sur différents niveaux, les bibliothèques pour le plus haut niveau, et le pilote directement pour le plus bas niveau. Une application programmée à plus bas niveau peut comporter des optimisations plus spécifiques, mais perd en généralisation.\\

\begin{figure}[h!]
  \centering
  \includegraphics[height=5cm,width=5cm]{images/niv-api.png}
\caption{Les différents niveaux}
\end{figure}

\subsubsection{Bibliothèques - Qualifieurs de fonctions}

\paragraph{}Ces bibliothèques présentent une extension des langages C, C++ et Fortran. Elles sont accompagnées d'un compilateur, $nvcc$ qui permet de compiler les fichiers les utilisant. Dans les bibliothèques CUDA le CPU est appelé $host$ et le GPU est appelé $device$. Les kernels sont les fonctions devant être exécutées parallèlement sur le GPU.

\paragraph{}Les fonctions doivent avoir une spécification supplémentaire indiquant par quel périphérique elles peuvent être appelées et par quel périphérique elles doivent être exécutées.

\begin{itemize}
\item Les fonctions appelées et exécutées par le CPU sont déclarées avec le mot clés \texttt{\_\_host\_\_}. Par défaut les fonctions sont déclarées en $host$ et donc ce mot clé n'est pas obligatoire.
\item Les fonctions appelées par le CPU et exécutées par le GPU doivent être déclarées avec le mot clé \texttt{\_\_global\_\_}.
\item Les fonctions appelées et exécutées par le GPU doivent être déclarées avec le mot clé \texttt{\_\_device\_\_}. Une fonction peut être en même temps $device$ et $host$.
\end{itemize}

\paragraph{}Lors de l'appel d'une fonction globale, il est nécessaire de préciser la taille de la grille et des blocs. Ces dimensions sont précisées entre triple chevrons, placés juste après le nom de la fonction. Chaque dimension doit être précisée à l'aide d'une instance de la structure dim3.\\

\fbox{
\begin{minipage}{1\textwidth}
\tt \_\_global\_\_ void mafonction(int a, int b) \{\\
\tt [...]\\
\tt \}\\
\tt [...]\\
\tt dim3 grilleD(3,3,1);\\
\tt dim3 blocD(3,3,3);\\
\tt mafonctionglobale $\textless$ $\textless$ $\textless$ grilleD,
blocD $\textgreater$  $\textgreater$  $\textgreater$ (a, b);\\
\end{minipage}
}

\subsubsection{Bibliothèque - Qualifieurs de variables}

Les variables peuvent être stockées dans les différentes mémoires de la carte graphique. Pour indiquer explicitement où stocker une variable il suffit lors de la déclaration d'ajouter avant le type l'un des qualifieurs suivants :
\begin{itemize}
\item \texttt{\_\_device\_\_} : variable stockée dans la mémoire globale,
\item \texttt{\_\_constant\_\_} : constante stockée dans la mémoire constante,
\item \texttt{\_\_shared\_\_} : variable stockée dans la mémoire shared.
\end{itemize}

\subsubsection{Bibliothèque - autre}

La bibliothèque propose de nombreuses autres fonctions pour synchroniser les threads d'un bloc ou encore accèder à des fonctions mathématiques plus complexes.

\section{Les matrices de convolution}

La plupart des filtres de traitement des images utilisent des matrices de convolution. Elles permettent, entre autre, de faire des flous, de la détection de bord, des translations, ... Mais l'utilité des matrices de convolutions ne s'arrête pas au domaine du traitement des images, de nombreux problèmes peuvent être réduits à une matrice de convolution.

\subsection{Définition}

Le principe d'une matrice de convolution est simple. Il s'agit de définir
une matrice dite ``de convolution'' et de la ``passer'' sur la
matrice de données en calculant successivement (pour chaque
pixels/données) la valeur résultante.

\begin{figure}[h!]
  \centering
  \includegraphics[]{images/convolution-calculate.png}
\caption{Exemple simple de convolution}
\end{figure}

L'avantage de cette méthode est qu'il est possible de diviser nos
données et d'effectuer les calculs en parallèle. En effet, il n'y a
pas de lien direct entre chaque données, il faut uniquement gérer les
bords lors du découpage.

\newpage
\subsection{Les algorithmes assimilés}

Les algorithmes assimilés à un calcul de matrice de convolution
nécessitent une étape supplémentaire, elle consiste à calculer la
matrice de convolution à appliquer sur la matrice en entrée pour avoir
le résultat escompté. De cette manière, il est possible de résoudre
les problèmes suivants en les résumant à l'application d'une
convolution :
\begin{itemize}
\item Le calcul d'écoulement de fluide,
\item Le calcul de diffusion de chaleur,
\item La détection de bord (filtre de Sobel).\\
\end{itemize}

Ce qui donnerait pour un écoulement des fluides une décomposition de
l'algorithme de la manière suivante :\\

\fbox{
\begin{minipage}{1\textwidth}
\tt void calculeEcoulementFluide(int** entree, int viscosite, int advection) \{\\
\tt \\
\tt int** matConvolutionFluide = CalculerMatConvolutionFluide(viscosite, advection);\\
\tt \\
\tt [...]\\
\tt \\
\tt ConvolEngine engine(matConvolutionFluide);\\
\tt engine.convol(entree);\\
\tt \\
\tt [...]\\
\tt \\
\tt \}\\
\end{minipage}
}

\paragraph{}Il parait donc pertinent d'appliquer des traitements par matrice de convolution de manière massivement parallèle sur architecture GPU.
