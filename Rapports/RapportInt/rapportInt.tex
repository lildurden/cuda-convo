\documentclass[a4paper,11pt]{report}
\usepackage[a4paper]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[francais]{babel}
\usepackage{hyperref}
\usepackage{pgf}
\usepackage{tikz}
\usepackage{gantt}
\usepackage{pdflscape}
\usetikzlibrary{arrows,automata}
\usepackage{amssymb}
\hypersetup{urlcolor=blue,linkcolor=black,citecolor=black,colorlinks=true} 
\usepackage[french,boxruled,inoutnumbered]{algorithm2e}	


%%%% debut macro pour enlever le nom chapitre %%%%
\makeatletter
\def\@makechapterhead#1{
  \vspace*{0\p@}
          {\parindent \z@ \raggedright \normalfont
            \interlinepenalty\@M
            \ifnum \c@secnumdepth >\m@ne
            \Huge\bfseries \thechapter\quad
            \fi
            \Huge \bfseries #1\par\nobreak
            \vskip 40\p@
}}

\def\@makeschapterhead#1{
  \vspace*{50\p@}
          {\parindent \z@ \raggedright
            \normalfont
            \interlinepenalty\@M
            \Huge \bfseries  #1\par\nobreak
            \vskip 40\p@
}}
\makeatother
%%%% fin macro %%%%


\title{TER\\Développement et tests \\d'algorithmes sur architectures
  parallèles\\--\\Rapport intermédiaire}
\author{Victor ALLOMBERT, Allan BLANCHARD\\ Adrien CARTERON, Vivien PELLETIER}

\begin{document}

\maketitle
\tableofcontents

\nocite{*}

\include{resume}
\include{intro}
\include{etatart}
\include{besoins_f}
\include{besoins_nf}
\include{protos}
\include{planning}

\bibliographystyle{plain}
\bibliography{biblio.bib}

\end{document}
