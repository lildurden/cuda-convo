\select@language {french}
\contentsline {chapter}{\numberline {1}R\IeC {\'e}sum\IeC {\'e} du projet}{3}{chapter.1}
\contentsline {paragraph}{}{3}{section*.2}
\contentsline {paragraph}{}{3}{section*.3}
\contentsline {paragraph}{}{3}{section*.4}
\contentsline {paragraph}{}{3}{section*.5}
\contentsline {chapter}{\numberline {2}Introduction au domaine}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}CUDA}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Les origines de CUDA}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Architecture}{4}{subsection.2.1.2}
\contentsline {subsubsection}{Hi\IeC {\'e}rarchisation des threads}{4}{section*.6}
\contentsline {paragraph}{}{4}{section*.7}
\contentsline {paragraph}{}{5}{section*.8}
\contentsline {subsubsection}{R\IeC {\'e}partition des m\IeC {\'e}moires}{6}{section*.9}
\contentsline {paragraph}{}{6}{section*.10}
\contentsline {paragraph}{}{6}{section*.11}
\contentsline {paragraph}{}{6}{section*.12}
\contentsline {paragraph}{}{6}{section*.13}
\contentsline {paragraph}{}{6}{section*.14}
\contentsline {subsection}{\numberline {2.1.3}Les outils fournis pour utiliser CUDA}{7}{subsection.2.1.3}
\contentsline {subsubsection}{Biblioth\IeC {\`e}ques - Qualifieurs de fonctions}{8}{section*.15}
\contentsline {paragraph}{}{8}{section*.16}
\contentsline {paragraph}{}{8}{section*.17}
\contentsline {paragraph}{}{8}{section*.18}
\contentsline {subsubsection}{Biblioth\IeC {\`e}que - Qualifieurs de variables}{9}{section*.19}
\contentsline {subsubsection}{Biblioth\IeC {\`e}que - autre}{9}{section*.20}
\contentsline {section}{\numberline {2.2}Les matrices de convolution}{9}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}D\IeC {\'e}finition}{9}{subsection.2.2.1}
\contentsline {paragraph}{}{9}{section*.21}
\contentsline {paragraph}{}{9}{section*.22}
\contentsline {subsection}{\numberline {2.2.2}Les algorithmes assimil\IeC {\'e}s}{10}{subsection.2.2.2}
\contentsline {paragraph}{}{10}{section*.23}
\contentsline {chapter}{\numberline {3}Analyse de l'existant}{11}{chapter.3}
\contentsline {paragraph}{}{11}{section*.24}
\contentsline {section}{\numberline {3.1}Les pr\IeC {\'e}c\IeC {\'e}dents TER}{11}{section.3.1}
\contentsline {paragraph}{}{11}{section*.25}
\contentsline {paragraph}{}{11}{section*.26}
\contentsline {section}{\numberline {3.2}Les matrices de convolution}{11}{section.3.2}
\contentsline {paragraph}{}{11}{section*.27}
\contentsline {section}{\numberline {3.3}La parall\IeC {\'e}lisation}{12}{section.3.3}
\contentsline {paragraph}{}{12}{section*.28}
\contentsline {chapter}{\numberline {4}Besoins fonctionnels}{13}{chapter.4}
\contentsline {paragraph}{}{13}{section*.29}
\contentsline {paragraph}{}{13}{section*.30}
\contentsline {section}{\numberline {4.1}Fonction de convolution CUDA}{13}{section.4.1}
\contentsline {section}{\numberline {4.2}D\IeC {\'e}coupage de donn\IeC {\'e}es : }{13}{section.4.2}
\contentsline {section}{\numberline {4.3}Sobel, Ecoulement de fluide, Diffusion de Chaleur}{14}{section.4.3}
\contentsline {paragraph}{}{14}{section*.36}
\contentsline {section}{\numberline {4.4}Gestion des bords}{14}{section.4.4}
\contentsline {chapter}{\numberline {5}Besoins non fonctionnels}{15}{chapter.5}
\contentsline {paragraph}{}{15}{section*.40}
\contentsline {section}{\numberline {5.1}Langage de programmation et plateforme}{15}{section.5.1}
\contentsline {section}{\numberline {5.2}Interface utilisateur}{15}{section.5.2}
\contentsline {section}{\numberline {5.3}Facilit\IeC {\'e} d'utilisation}{15}{section.5.3}
\contentsline {section}{\numberline {5.4}Documentation}{16}{section.5.4}
\contentsline {chapter}{\numberline {6}Prototypes et Id\IeC {\'e}es}{17}{chapter.6}
\contentsline {paragraph}{}{17}{section*.49}
\contentsline {section}{\numberline {6.1}M\IeC {\'e}thode d'appel g\IeC {\'e}n\IeC {\'e}rique}{17}{section.6.1}
\contentsline {paragraph}{}{17}{section*.50}
\contentsline {paragraph}{}{17}{section*.52}
\contentsline {paragraph}{}{17}{section*.53}
\contentsline {paragraph}{}{17}{section*.55}
\contentsline {paragraph}{}{17}{section*.56}
\contentsline {paragraph}{}{18}{section*.58}
\contentsline {section}{\numberline {6.2}M\IeC {\'e}moire : partage et d\IeC {\'e}coupage}{18}{section.6.2}
\contentsline {paragraph}{}{18}{section*.59}
\contentsline {paragraph}{}{18}{section*.61}
\contentsline {paragraph}{}{18}{section*.62}
\contentsline {paragraph}{}{18}{section*.63}
\contentsline {paragraph}{}{18}{section*.65}
\contentsline {paragraph}{}{18}{section*.66}
\contentsline {paragraph}{}{19}{section*.67}
\contentsline {paragraph}{}{19}{section*.69}
\contentsline {paragraph}{}{19}{section*.70}
\contentsline {section}{\numberline {6.3}Tests et optimisations possibles}{19}{section.6.3}
\contentsline {paragraph}{}{19}{section*.71}
\contentsline {paragraph}{}{19}{section*.73}
\contentsline {paragraph}{}{19}{section*.75}
\contentsline {paragraph}{}{20}{section*.77}
\contentsline {paragraph}{}{20}{section*.78}
\contentsline {section}{\numberline {6.4}Evolution par rapport au prototype}{20}{section.6.4}
\contentsline {paragraph}{}{20}{section*.79}
\contentsline {paragraph}{}{20}{section*.80}
\contentsline {chapter}{\numberline {7}Exemple de fonctionnement}{21}{chapter.7}
\contentsline {paragraph}{}{21}{section*.81}
\contentsline {paragraph}{}{22}{section*.82}
\contentsline {paragraph}{}{22}{section*.83}
\contentsline {chapter}{\numberline {8}Architecture et Documentation}{23}{chapter.8}
\contentsline {paragraph}{}{23}{section*.84}
\contentsline {paragraph}{}{23}{section*.85}
\contentsline {section}{\numberline {8.1}Diagramme}{23}{section.8.1}
\contentsline {paragraph}{}{23}{section*.86}
\contentsline {paragraph}{}{23}{section*.87}
\contentsline {paragraph}{}{23}{section*.88}
\contentsline {section}{\numberline {8.2}Architecture}{24}{section.8.2}
\contentsline {paragraph}{}{24}{section*.89}
\contentsline {paragraph}{}{24}{section*.90}
\contentsline {paragraph}{}{24}{section*.91}
\contentsline {paragraph}{}{24}{section*.92}
\contentsline {section}{\numberline {8.3}Documentation}{25}{section.8.3}
\contentsline {paragraph}{}{25}{section*.93}
\contentsline {chapter}{\numberline {9}Convolution}{26}{chapter.9}
\contentsline {paragraph}{}{26}{section*.94}
\contentsline {section}{\numberline {9.1}G\IeC {\'e}n\IeC {\'e}rique}{26}{section.9.1}
\contentsline {section}{\numberline {9.2}S\IeC {\'e}quentiel}{27}{section.9.2}
\contentsline {section}{\numberline {9.3}Parall\IeC {\`e}le - Multi-Thread}{28}{section.9.3}
\contentsline {paragraph}{}{28}{section*.95}
\contentsline {paragraph}{}{28}{section*.96}
\contentsline {section}{\numberline {9.4}Parall\IeC {\`e}le - CUDA}{28}{section.9.4}
\contentsline {paragraph}{}{28}{section*.97}
\contentsline {subsection}{\numberline {9.4.1}D\IeC {\'e}coupage}{28}{subsection.9.4.1}
\contentsline {paragraph}{}{28}{section*.98}
\contentsline {paragraph}{Caract\IeC {\'e}ristiques physiques}{29}{section*.99}
\contentsline {paragraph}{}{29}{section*.100}
\contentsline {paragraph}{}{29}{section*.101}
\contentsline {paragraph}{}{29}{section*.102}
\contentsline {paragraph}{Calcul des dimensions maximales}{29}{section*.103}
\contentsline {paragraph}{}{29}{section*.104}
\contentsline {paragraph}{}{29}{section*.105}
\contentsline {paragraph}{}{29}{section*.106}
\contentsline {paragraph}{}{29}{section*.107}
\contentsline {paragraph}{}{29}{section*.108}
\contentsline {paragraph}{}{29}{section*.109}
\contentsline {paragraph}{}{30}{section*.110}
\contentsline {paragraph}{Calcul des positions et des dimensions r\IeC {\'e}elles des blocs}{30}{section*.111}
\contentsline {paragraph}{}{30}{section*.112}
\contentsline {paragraph}{}{30}{section*.113}
\contentsline {paragraph}{}{30}{section*.114}
\contentsline {paragraph}{}{30}{section*.115}
\contentsline {paragraph}{}{30}{section*.116}
\contentsline {subsection}{\numberline {9.4.2}Streaming - utilisation Pinned-Memory}{31}{subsection.9.4.2}
\contentsline {paragraph}{Stream}{31}{section*.117}
\contentsline {paragraph}{}{31}{section*.118}
\contentsline {paragraph}{}{31}{section*.119}
\contentsline {paragraph}{}{31}{section*.120}
\contentsline {paragraph}{}{31}{section*.121}
\contentsline {paragraph}{Pinned Memory}{31}{section*.122}
\contentsline {paragraph}{}{31}{section*.123}
\contentsline {paragraph}{}{31}{section*.124}
\contentsline {subsection}{\numberline {9.4.3}Calcul - utilisation des Textures}{32}{subsection.9.4.3}
\contentsline {paragraph}{}{32}{section*.125}
\contentsline {paragraph}{}{32}{section*.126}
\contentsline {paragraph}{}{32}{section*.127}
\contentsline {paragraph}{}{32}{section*.128}
\contentsline {paragraph}{}{32}{section*.129}
\contentsline {paragraph}{}{32}{section*.130}
\contentsline {paragraph}{}{32}{section*.131}
\contentsline {subsection}{\numberline {9.4.4}Calcul - utilisation de la m\IeC {\'e}moire Shared}{32}{subsection.9.4.4}
\contentsline {paragraph}{}{32}{section*.132}
\contentsline {paragraph}{}{32}{section*.133}
\contentsline {paragraph}{}{33}{section*.134}
\contentsline {paragraph}{}{33}{section*.135}
\contentsline {paragraph}{}{33}{section*.136}
\contentsline {paragraph}{}{33}{section*.137}
\contentsline {paragraph}{}{34}{section*.138}
\contentsline {paragraph}{}{34}{section*.139}
\contentsline {subsection}{\numberline {9.4.5}Principaux probl\IeC {\`e}mes rencontr\IeC {\'e}s}{34}{subsection.9.4.5}
\contentsline {paragraph}{}{34}{section*.140}
\contentsline {paragraph}{}{34}{section*.141}
\contentsline {paragraph}{}{34}{section*.142}
\contentsline {chapter}{\numberline {10}Complexit\IeC {\'e}}{35}{chapter.10}
\contentsline {section}{\numberline {10.1}S\IeC {\'e}quentiel}{35}{section.10.1}
\contentsline {paragraph}{}{35}{section*.143}
\contentsline {paragraph}{}{35}{section*.144}
\contentsline {section}{\numberline {10.2}Parall\IeC {\`e}le : Processeur}{36}{section.10.2}
\contentsline {paragraph}{}{36}{section*.145}
\contentsline {section}{\numberline {10.3}Parall\IeC {\`e}le : GPU}{36}{section.10.3}
\contentsline {paragraph}{}{36}{section*.146}
\contentsline {paragraph}{}{36}{section*.147}
\contentsline {paragraph}{}{36}{section*.148}
\contentsline {paragraph}{}{37}{section*.149}
\contentsline {chapter}{\numberline {11}V\IeC {\'e}rifications et tests}{38}{chapter.11}
\contentsline {section}{\numberline {11.1}Benchmarks}{38}{section.11.1}
\contentsline {paragraph}{}{38}{section*.150}
\contentsline {paragraph}{}{38}{section*.151}
\contentsline {paragraph}{}{38}{section*.152}
\contentsline {paragraph}{}{39}{section*.153}
\contentsline {section}{\numberline {11.2}V\IeC {\'e}rification des r\IeC {\'e}sultats}{39}{section.11.2}
\contentsline {paragraph}{}{39}{section*.154}
\contentsline {section}{\numberline {11.3}Qu'en d\IeC {\'e}duire ?}{39}{section.11.3}
\contentsline {paragraph}{}{39}{section*.155}
\contentsline {paragraph}{}{39}{section*.156}
\contentsline {chapter}{\numberline {12}Description des extensions possibles}{40}{chapter.12}
\contentsline {paragraph}{}{40}{section*.157}
\contentsline {section}{\numberline {12.1}Personnalisation de la matrice et sa formule}{40}{section.12.1}
\contentsline {paragraph}{}{40}{section*.158}
\contentsline {section}{\numberline {12.2}Gestion des bords}{40}{section.12.2}
\contentsline {paragraph}{}{40}{section*.159}
\contentsline {section}{\numberline {12.3}Les probl\IeC {\`e}mes qui peuvent se r\IeC {\'e}duire \IeC {\`a} la convolution}{40}{section.12.3}
\contentsline {paragraph}{}{40}{section*.160}
\contentsline {section}{\numberline {12.4}Am\IeC {\'e}lioration des performances}{40}{section.12.4}
\contentsline {paragraph}{}{40}{section*.161}
\contentsline {paragraph}{}{41}{section*.162}
