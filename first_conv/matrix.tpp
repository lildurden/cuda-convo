///\file matrix.tpp
///\brief contient les implémentations des méthodes de Matrix
///\author A.Blanchard
#include <cassert>

template<class T>
Matrix<T>::Matrix(unsigned int width, unsigned int height):w(width), h(height){
  assert(height != 0);
  assert(width  != 0);

  container = new T[w*h];
}

template<class T>
Matrix<T>::Matrix(const Matrix<T> &m):w(m.w), h(m.h){
  container = new T[w*h];
  for(unsigned int i = 0; i < w*h; i++)
    container[i] = m.container[i];
}

template<class T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T> &m){
  if(&m != this){
    if(container) delete container;

    w = m.w;
    h = m.h;

    container = new T[w*h];
    for(unsigned int i(0); i < w*h; i++)
      container[i] = m.container[i];
  }
  return *this;
}

template<class T>
Matrix<T>::~Matrix(){
  if(container) delete container;
}
