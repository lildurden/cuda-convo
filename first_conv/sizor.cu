///\file sizor.cu
///\brief contient les implémentations de la classe CUDA::Sizor
///\author { V.Allombert, A.Blanchard, A.Carteron, V.Pelletier } 
#include "sizor.hh"

namespace CUDA{

  Sizor::Sizor(const Image &data, const Matrix<char> &m_conv){
  
    unsigned int start=m_conv.width()/2;

    hblockmax_out = 32*MPS;
    hblock_in = hblockmax_out+(2*start); 

    wblock_in = MAXMEM/(hblock_in*sizeof(pixel));
    wblockmax_out = wblock_in-(2*start);

    block_size = wblock_in*hblock_in;

    unsigned int x = (data.width()-2*start)/wblockmax_out + (((data.width()-2*start)%wblockmax_out) > 0);
    unsigned int y = (data.height()-2*start)/hblockmax_out + (((data.height()-2*start)%hblockmax_out) > 0);
    
    for(unsigned int i = 0; i < y; i++)
      for(unsigned int j = 0; j < x; j++){
	coord_data c;
	
	c.bxo = start+j*wblockmax_out;
	c.bxi = c.bxo-start;

	c.byo = start+i*hblockmax_out;
	c.byi = c.byo-start;
	
	if(j == x-1)
	  c.wblock_out = data.width()-start-c.bxo;
	else
	  c.wblock_out = wblockmax_out;

	if(i == y-1)
	  c.hblock_out = data.height()-start-c.byo;
	else
	  c.hblock_out = hblockmax_out; //POULET

	c.hblock_in = c.hblock_out+2*start;
	c.wblock_in = c.wblock_out+2*start;
 
	std::cout<<c.bxi<<"("<<j<<"):"<<c.byi<<"("<<i<<")"<<std::endl;
	std::cout<<c.bxo<<"("<<j<<"):"<<c.byo<<"("<<i<<")"<<std::endl;
	std::cout<<c.wblock_in<<"/"<<c.hblock_in<<std::endl;

	block_lst.push_back(c);

      }

    it=block_lst.begin();
  }


}
