///\file matrix.hh
///\brief contient les spécifications de la class Matrix
///\author A.Blanchard
#ifndef _MATRIX
#define _MATRIX

///\class Matrix
///\brief définit les matrices génériques
///\param T Type de matrice à créer
template<class T>
class Matrix{
protected:
  T* container;   ///< tableau une dimension d'éléments
  unsigned int w; ///< largeur de la matrice
  unsigned int h; ///< hauteur de la matrice   

  ///\brief Constructeur vide
  ///On autorise pas la création d'une matrice sans dimensions
  Matrix();

public:

  ///\brief Constructeur
  ///construction d'une matrice définie par sa taille
  ///\param width largeur de la matrice
  ///\param height hauteur de la matrice
  Matrix(unsigned int width, unsigned int height);

  ///\brief Constructeur de copie
  ///construction d'une matrice à partir d'une autre
  ///\param m matrice à copier
  Matrix(const Matrix &m);
  
  ///\brief opérateur d'affectation
  ///met le contenu de m dans *this
  ///\param m matrice à affecter
  ///\return reference sur *this
  Matrix& operator=(const Matrix &m);

  ///\brief Destructeur
  ///libère la mémoire demandée par la matrice
  virtual ~Matrix();
  
  ///\brief getter sur w
  ///\return largeur de la matrice
  inline unsigned int width() const { return w; }

  ///\brief getter sur h
  ///\return hauteur de la matrice
  inline unsigned int height() const { return h; }

  ///\brief donne un accès au début d'une ligne de la matrice
  ///\param i numéro de la ligne à obtenir
  ///\return pointeur sur le début de la ligne i
  inline T* operator[](unsigned int i) const{ return &container[i*w]; }
};

#include "matrix.tpp"

#endif
