\beamer@endinputifotherversion {3.26pt}
\select@language {french}
\beamer@sectionintoc {1}{Domaine}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{La programmation massivement multiprocesseur}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{Les matrices de convolution}{9}{0}{1}
\beamer@sectionintoc {2}{Description}{11}{0}{2}
\beamer@sectionintoc {3}{Architecture}{13}{0}{3}
\beamer@sectionintoc {4}{Points techniques}{17}{0}{4}
\beamer@subsectionintoc {4}{1}{Streaming}{18}{0}{4}
\beamer@subsectionintoc {4}{2}{Optimisations M\IeC {\'e}moire}{21}{0}{4}
\beamer@sectionintoc {5}{Fonctionnement}{25}{0}{5}
\beamer@sectionintoc {6}{R\IeC {\'e}sultats}{29}{0}{6}
